﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutoCompleter;

namespace AutoCompleterTests
{
    [TestClass]
    public class DictionaryTests
    {
        private Dictionary dict;

        public DictionaryTests()
        {
            this.dict = new Dictionary(6, d => {
                foreach (var item in new string[] { "ba", "bb", "c", "aa", "aab", "aba" })
                {
                    d.Add(item, 0);
                }
            });
        }

        [TestMethod]
        public void ShouldBeSorted()
        {
            Assert.AreEqual("aa", dict.Get(0).value);
            Assert.AreEqual("aab", dict.Get(1).value);
            Assert.AreEqual("aba", dict.Get(2).value);
            Assert.AreEqual("ba", dict.Get(3).value);
            Assert.AreEqual("bb", dict.Get(4).value);
            Assert.AreEqual("c", dict.Get(5).value);
        }

        [TestMethod]
        public void Get_ShouldReturnWordByIndex()
        {
            Assert.AreEqual("aa", dict.Get(0).value);
            Assert.AreEqual("aba", dict.Get(2).value);
            Assert.AreEqual("c", dict.Get(5).value);
        }

        [TestMethod]
        public void Length_ShouldReturnActualSize()
        {
            Assert.AreEqual(6, dict.Length());
        }

        [TestMethod]
        [ExpectedException(typeof(Dictionary.SizeLimitExceededException))]
        public void Add_ShouldThrowSizeLimitExceeded_WhenOverflow()
        {
            dict.Add("d", 0);
        }

        [TestMethod]
        [ExpectedException(typeof(Dictionary.InvalidRangeException))]
        public void FindRange_ShouldThrowInvalidRange_WhenLeftIndexLessThan0()
        {
            dict.FindRange("aa", -1, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(Dictionary.InvalidRangeException))]
        public void FindRange_ShouldThrowInvalidRange_WhenRightIndexGreaterOrEqualSize()
        {
            dict.FindRange("aa", 0, 6);
        }

        [TestMethod]
        public void FindRange_Example1()
        {
            int[] result = dict.FindRange("a");
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }

        [TestMethod]
        public void FindRange_Example2()
        {
            int[] result = dict.FindRange("aa");
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(1, result[1]);
        }

        [TestMethod]
        public void FindRange_Example3()
        {
            int[] result = dict.FindRange("b");
            Assert.AreEqual(3, result[0]);
            Assert.AreEqual(4, result[1]);
        }

        [TestMethod]
        public void FindRange_Example4()
        {
            int[] result = dict.FindRange("bb");
            Assert.AreEqual(4, result[0]);
            Assert.AreEqual(4, result[1]);
        }

        [TestMethod]
        public void FindRange_Example5()
        {
            int[] result = dict.FindRange("c");
            Assert.AreEqual(5, result[0]);
            Assert.AreEqual(5, result[1]);
        }

        [TestMethod]
        public void FindRange_Example6()
        {
            int[] result = dict.FindRange("d");
            Assert.AreEqual(-1, result[0]);
            Assert.AreEqual(-1, result[1]);
        }
    }
}
