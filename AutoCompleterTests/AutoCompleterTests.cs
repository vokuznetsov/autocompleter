﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace AutoCompleterTests
{
    [TestClass]
    public class AutoCompleterTests
    {
        private AutoCompleter.AutoCompleter autocompleter;

        public AutoCompleterTests()
        {
            AutoCompleter.Dictionary dict = new AutoCompleter.Dictionary(6, d => {
                d.Add("kare", 10);
                d.Add("kanojo", 20);
                d.Add("karetachi", 1);
                d.Add("korosu", 7);
                d.Add("koroso", 7);
                d.Add("sakura", 3);

            });

            autocompleter = new AutoCompleter.AutoCompleter(dict);
        }

        [TestMethod]
        public void Completions_Example1()
        {
            List<string> expected = new List<string>(new string[] { "kanojo", "kare", "koroso", "korosu", "karetachi" });
            CollectionAssert.AreEqual(expected, autocompleter.Completions("k", 10));
        }

        [TestMethod]
        public void Completions_Example2()
        {
            List<string> expected = new List<string>(new string[] { "kanojo", "kare", "karetachi" });
            CollectionAssert.AreEqual(expected, autocompleter.Completions("ka", 10));
        }

        [TestMethod]
        public void Completions_Example3()
        {
            List<string> expected = new List<string>(new string[] { "kare", "karetachi" });
            CollectionAssert.AreEqual(expected, autocompleter.Completions("kar", 10));
        }
    }
}
