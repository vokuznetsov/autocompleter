﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AutoCompleter;

namespace AutoCompleterTests
{
    [TestClass]
    public class WordsSegmentTreeTests
    {
        private WordsSegmentTree stree;

        public WordsSegmentTreeTests()
        {
            Dictionary dict = new Dictionary(4, d => {
                d.Add("a", 20);
                d.Add("b", 10);
                d.Add("c", 12);
                d.Add("d", 24);
            });

            stree = new WordsSegmentTree(dict);
        }

        [TestMethod]
        [ExpectedException(typeof(WordsSegmentTree.InvalidRangeException))]
        public void QueryMax_ShouldThrowInvalidRange_WhenLowIndexLessThan0()
        {
            stree.QueryMax(-1, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(WordsSegmentTree.InvalidRangeException))]
        public void QueryMax_ShouldThrowInvalidRange_WhenHighIndexGreaterOrEqLength()
        {
            stree.QueryMax(0, 4);
        }

        [TestMethod]
        public void QueryMax_Example1()
        {
            Assert.AreEqual(24, stree.QueryMax(0, 3).weight);
        }

        [TestMethod]
        public void QueryMax_Example2()
        {
            Assert.AreEqual(20, stree.QueryMax(0, 1).weight);
        }

        [TestMethod]
        public void QueryMax_Example3()
        {
            Assert.AreEqual(12, stree.QueryMax(1, 2).weight);
        }

        [TestMethod]
        public void QueryMax_Example4()
        {
            Assert.AreEqual(20, stree.QueryMax(0, 0).weight);
        }
    }
}
