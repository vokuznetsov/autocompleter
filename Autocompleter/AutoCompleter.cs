﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Priority_Queue;

namespace AutoCompleter
{
    /// <summary>
    /// Autocompleter. Returns the words from dictionary that match the specified prefix sorted by weight
    /// </summary>
    public class AutoCompleter
    {
        /// <summary>
        /// Internal structure that store intermediate result
        /// </summary>
        private class PhraseRange : PriorityQueueNode
        {
            public int first, last;
            public int index;

            public PhraseRange(int first, int last, int index)
            {
                this.first = first;
                this.last = last;
                this.index = index;
            }
        }

        private Dictionary dict;
        private WordsSegmentTree stree;

        /// <summary>
        /// Instantiate a new Autocompleter
        /// </summary>
        /// <param name="dict">Dictionary</param>
        public AutoCompleter(Dictionary dict)
        {
            this.dict = dict;
            this.stree = new WordsSegmentTree(dict);
        }

        /// <summary>
        /// Find completions
        /// </summary>
        /// <param name="prefix">Prefix to search</param>
        /// <param name="count">Max number of completions</param>
        /// <returns>List of completions</returns>
        public List<string> Completions(string prefix, int count)
        {
            HeapPriorityQueue<PhraseRange> queue = new HeapPriorityQueue<PhraseRange>(count * 2);
            List<string> results = new List<string>();

            int[] first_last = dict.FindRange(prefix);
            int first = first_last[0];
            int last = first_last[1];

            if (first == -1 && last == -1) return results;

            Word best_word = stree.QueryMax(first, last);
            queue.Enqueue(new PhraseRange(first, last, best_word.index), Priority(best_word));
            --last;

            for(int i = 0; i < count && queue.Count != 0; i++) 
            {
                PhraseRange pr = queue.Dequeue();

                results.Add(dict.Get(pr.index).value);

                int lower = pr.first;
                int upper = pr.index - 1;

                if (lower <= upper)
                {
                    best_word = stree.QueryMax(lower, upper);
                    queue.Enqueue(new PhraseRange(lower, upper, best_word.index), Priority(best_word));
                }

                lower = pr.index + 1;
                upper = pr.last;

                if (lower <= upper)
                {
                    best_word = stree.QueryMax(lower, upper);
                    queue.Enqueue(new PhraseRange(lower, upper, best_word.index), Priority(best_word));
                }

            }

            return results;
        }

        /// <summary>
        /// Returns word sorting priority
        /// </summary>
        private int Priority(Word word) {
            return -word.weight * dict.Length() + word.index;
        }
       
    }
}
