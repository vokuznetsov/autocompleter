﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompleter
{
    /// <summary>
    /// Dictionary stores words and their weight.
    /// Allows you to find the left and right index of the range of words that begin with the prefix.
    /// </summary>
    /// <example>
    /// <code>
    ///   Dictionary dict = new Dictionary(5, dict => {
    ///       d.Add("aa", 2);
    ///       d.Add("ab", 3);
    ///       d.Add("ac", 1);
    ///       d.Add("bb", 4);
    ///       d.Add("ba", 3);
    ///   });
    ///
    ///   dict.FindRange("a");  // returns int[] { 0, 2 }
    ///   dict.FindRange("bb"); // returns int[] { 3, 3 }
    ///   dict.FindRange("dd"); // returns int[] { -1, -1 }
    /// </code>
    /// </example>
    public class Dictionary
    {
        private Word[] words;
        private int count = 0;

        public class SizeLimitExceededException : Exception { }
        public class InvalidRangeException : Exception { }

        /// <summary>
        /// Instantiate a new Dictionary
        /// </summary>
        /// <param name="size">Max number of words</param>
        /// <param name="action">Lambda that adds items</param>
        public Dictionary(int size, Action<Dictionary> action)
        {
            words = new Word[size];
            action.Invoke(this);
            Sort();
        }

        /// <summary>
        /// Get element by index
        /// </summary>
        public Word Get(int index)
        {
            return words[index];
        }

        /// <summary>
        /// Returns the number of words
        /// </summary>
        public int Length()
        {
            return count;
        }

        /// <summary>
        /// Add word
        /// </summary>
        /// <param name="word">Word</param>
        /// <param name="weight">Weight of word</param>
        public void Add(string word, int weight)
        {
            if (count == words.Length) throw new SizeLimitExceededException();

            words[count] = new Word(word, count, weight);

            count++;
        }

        /// <summary>
        /// Sort words and update their indexes
        /// </summary>
        public void Sort()
        {
            Array.Sort(words, (word1, word2) => word1.value.CompareTo(word2.value));

            for (int i = 0; i < words.Length; i++)
            {
                words[i].index = i;
            }
        }

        /// <summary>
        /// Locate the beginning and end of candidate list of phrases
        /// </summary>
        /// <param name="prefix">Prefix for searching</param>
        /// <param name="low">Initial low index</param>
        /// <param name="high">Initial high index</param>
        /// <returns>Low and high indexes</returns>
        public int[] FindRange(string prefix, int low = 0, int high = -1)
        {
            if (low < 0 || high >= Length()) throw new InvalidRangeException();

            if (high == -1) high = words.Length - 1;
            if (high < low) return new int[] { -1, -1 };

            int mid = (low + high) / 2;
            string midValue = words[mid].value;

            switch (prefix.CompareTo(midValue.Substring(0, prefix.Length)))
            {
                case +1:
                    return FindRange(prefix, mid + 1, high);
                case -1:
                    return FindRange(prefix, low, mid - 1);
                default:
                    return new int[] { LeftIndex(prefix, low, high), RightIndex(prefix, low, high) };
            }
        }

        private int LeftIndex(string prefix, int low, int high)
        {
            if (high < low) return -1;

            int mid = (low + high) / 2;
            string midValue = words[mid].value;

            switch (prefix.CompareTo(midValue.Substring(0, prefix.Length)))
            {
                case +1:
                    return LeftIndex(prefix, mid + 1, high);
                case -1:
                    return LeftIndex(prefix, low, mid - 1);
                default:
                    if (mid == 0 || !words[mid - 1].value.StartsWith(prefix))
                        return mid;
                    else
                        return LeftIndex(prefix, low, mid - 1);
            }
        }

        private int RightIndex(string prefix, int low, int high)
        {
            if (high < low) return -1;

            int mid = (low + high) / 2;
            string midValue = words[mid].value;

            switch (prefix.CompareTo(midValue.Substring(0, prefix.Length)))
            {
                case +1:
                    return RightIndex(prefix, mid + 1, high);
                case -1:
                    return RightIndex(prefix, low, mid - 1);
                default:
                    if (mid == words.Length - 1 || !words[mid + 1].value.StartsWith(prefix))
                        return mid;
                    else
                        return RightIndex(prefix, mid + 1, high);
            }
        }
    }
}
