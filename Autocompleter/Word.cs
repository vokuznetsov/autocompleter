﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompleter
{   
    public struct Word
    {
        public string value;
        public int index;
        public int weight;

        public Word(string value, int index, int weight)
        {
            this.value = value;
            this.index = index;
            this.weight = weight;
        }
    }
}
