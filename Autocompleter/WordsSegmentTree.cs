﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompleter
{
    /// <summary>
    /// Segment Tree that stores the maximum weight of the segment at every node
    /// </summary>
    public class WordsSegmentTree
    {
        private Word[] tree;
        private int length;

        public class InvalidRangeException : Exception { }

        /// <summary>
        /// Instantiate a new WordsSegmentTree
        /// </summary>
        /// <param name="dict">Dictionary</param>
        public WordsSegmentTree(Dictionary dict)
        {
            length = dict.Length();
            tree = new Word[length * 4];
            Build(dict, 1, 0, length - 1);
        }

        /// <summary>
        /// Find word with maximum weight on specified range
        /// </summary>
        /// <param name="begin">Index of the beginning of the range in which to search</param>
        /// <param name="end">Index of the end of the range in which to search</param>
        /// <returns>Word with maximum weight</returns>
        public Word QueryMax(int begin, int end)
        {
            if (begin < 0 || end >= length) throw new InvalidRangeException();
            return QueryMax(1, 0, length - 1, begin, end);
        }

        /// <summary>
        /// Find word with maximum weight on segment
        /// </summary>
        /// <param name="node_num">Number of current node</param>
        /// <param name="low">Low index of current segment</param>
        /// <param name="high">High index of current segment</param>
        /// <param name="begin">Index of the beginning of the range in which to search</param>
        /// <param name="end">Index of the end of the range in which to search</param>
        /// <returns>Word with maximum weight</returns>
        private Word QueryMax(int node_num, int low, int high, int begin, int end)
        {
            if (end < begin) return new Word();

            if (end == high && begin == low)
                return tree[node_num];

            int mid = (low + high) / 2;

            Word w1 = QueryMax(node_num * 2, low, mid, begin, Math.Min(end, mid));
            Word w2 = QueryMax(node_num * 2 + 1, mid + 1, high, Math.Max(begin, mid + 1), end);

            return (w1.weight >= w2.weight) ? w1 : w2;
        }

        /// <summary>
        /// Recursively build segment tree
        /// </summary>
        /// <param name="dict">Dictionary</param>
        /// <param name="node_num">Number of current node</param>
        /// <param name="low">Low index of current segment</param>
        /// <param name="high">High index of current segment</param>
        private void Build(Dictionary dict, int node_num, int low, int high)
        {
            if (low == high)
                tree[node_num] = dict.Get(low);
            else
            {
                int mid = (low + high) / 2;
                Build(dict, node_num * 2, low, mid);
                Build(dict, node_num * 2 + 1, mid + 1, high);

                Word w1 = tree[node_num * 2];
                Word w2 = tree[node_num * 2 + 1];

                tree[node_num] = (w1.weight >= w2.weight) ? w1 : w2;
            }
        }
    }
}
