﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AutoCompleter
{
    class Program
    {
        private const int SUGGESTION_COUNT = 10;

        static void Main(string[] args)
        {
            int lineCount = Int32.Parse(Console.ReadLine());

            Dictionary dict = new Dictionary(lineCount, d => {
                for (int i = 0; i < lineCount; i++)
                {
                    string[] word_and_weight = Console.ReadLine().Split(' ');
                    d.Add(word_and_weight[0], Int32.Parse(word_and_weight[1]));
                }
            });

            int prefixCount = Int32.Parse(Console.ReadLine());
            AutoCompleter ac = new AutoCompleter(dict);

            for (int i = 0; i < prefixCount; i++)
            {
                foreach (var suggestion in ac.Completions(Console.ReadLine(), SUGGESTION_COUNT))
                {
                    Console.WriteLine(suggestion);
                }
                if (i < prefixCount - 1) Console.WriteLine();
            }

        }
    }
}
